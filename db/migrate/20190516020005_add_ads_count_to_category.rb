class AddAdsCountToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :ads_count, :interger, default: 0
  end
end
