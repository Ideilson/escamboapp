namespace :dev do

  desc "Setup Development"
  task setup: :environment do
    images_path = Rails.root.join('public','system')
    puts "Setting ENVIRONMENT... #{%x(RAILS_ENV=development)}"
    puts "Deleting images...#{%x(rm -rf #{images_path})}"
    puts "Dropping DB... #{%x(rake db:drop)}"
    puts "Creating DB... #{%x(rake db:create)}"
    puts %x(rake db:migrate)
    puts %x(rake db:seed)
    puts %x(rake dev:generate_admins)
    puts %x(rake dev:generate_members)
    puts %x(rake dev:generate_ads)
    puts "Setup completed with success!"
  end

  #################################################################

  desc "Create fake Administrators"
  task generate_admins: :environment do
    puts "Creating admins..."
    10.times do
      Admin.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        password: "123456",
        password_confirmation: "123456",
        role: [0,0,1,1,1,1,1].sample
      )
    end
    puts "Created admins!"
  end

  #################################################################

  desc "Create fake Members"
  task generate_members: :environment do
    puts "Creating Members..."
    100.times do
      Member.create!(
        email: Faker::Internet.email,
        password: "123456",
        password_confirmation: "123456"
      )
    end
    puts "Created Members!"
  end

  #################################################################

  desc "Create Ads Fake"
  task generate_ads: :environment do
    puts "Creating Ads..."

    5.times do
      Ad.create!(
        title: Faker::Lorem.sentence([2,3,4,5].sample),
        description_md: markdown_fake,
        description_short: Faker::Lorem.sentence([2,3].sample),
        category: Category.all.sample,
        member: Member.first,
        price: "#{Random.rand(500)},#{Random.rand(99)}",
        finish_date: Date.today + Random.rand(90),
        picture: File.new(Rails.root.join('public','template','images-for-ads',"#{Random.rand(9)}.jpg"), 'r')
      )
    end

    100.times do
      Ad.create!(
        title: Faker::Lorem.sentence([2,3,4,5].sample),
        description_md: markdown_fake,
        description_short: Faker::Lorem.sentence([2,3].sample),
        category: Category.all.sample,
        member: Member.all.sample,
        price: "#{Random.rand(500)},#{Random.rand(99)}",
        finish_date: Date.today + Random.rand(90),
        picture: File.new(Rails.root.join('public','template','images-for-ads',"#{Random.rand(9)}.jpg"), 'r')
      )
    end
    puts "Ads created!"
  end

  def markdown_fake
    %x(ruby -e "require 'doctor_ipsum'; puts DoctorIpsum::Markdown.entry")
  end

end
