class Admin < ActiveRecord::Base
  # CONSTANTS
  ROLES = {:full_access => 0, :restrict_access => 1}

  # ENUMS
  enum role: ROLES
  # enum role: {:full_access => 0, :restrict_access => 1}

  #SCOPES
  scope :with_full_access, -> { where(role: ROLES[:full_access]) }
  scope :with_restrict_access, -> { where(role: ROLES[:restrict_access]) }

  # scope :with_full_access, -> { where(role: Admin.roles[:full_access]) }
  # scope :with_restrict_access, -> { where(role: Admin.roles[:full_access]) }


  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
